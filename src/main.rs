use inotify::{EventMask, Inotify, WatchMask};
use serde_derive::{Deserialize, Serialize};
use url::Url;

const APP_NAME: &str = "webcam-light";

#[derive(Default, Debug, Serialize, Deserialize)]
struct Config {
    device: std::path::PathBuf,
    on_url: String,
    off_url: String,
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let mut config_valid = true;

    let cfg: Config = confy::load(APP_NAME)?;

    let on_url = Url::parse(&cfg.on_url);
    let off_url = Url::parse(&cfg.on_url);

    if let Err(e) = on_url {
        config_valid = false;
        eprintln!("Failed to parse on_url: {}", e);
    }

    if let Err(e) = off_url {
        config_valid = false;
        eprintln!("Failed to parse off_url: {}", e);
    }

    if !config_valid {
        std::process::exit(1);
    }

    let mut inotify = Inotify::init().expect("Failed to initialize inotify");
    let mut camera_in_use: bool = false;
    let mut previous_mask: Option<EventMask> = None;

    inotify
        .add_watch(cfg.device, WatchMask::ALL_EVENTS)
        .expect("Failed to add inotify watch");

    println!("Watching for webcam activity");

    let mut buffer = [0u8; 4096];

    loop {
        let events = inotify
            .read_events_blocking(&mut buffer)
            .expect("Failed to read inotify events");

        for event in events {
            if event.mask.contains(EventMask::OPEN) && !camera_in_use {
                if let Some(EventMask::OPEN) = previous_mask {
                    println!("Camera in use");
                    camera_in_use = true;
                    reqwest::get(&cfg.on_url).await?;
                }
            } else if event.mask.contains(EventMask::CLOSE_WRITE) && camera_in_use {
                println!("Camera not in use");
                reqwest::get(&cfg.off_url).await?;
                camera_in_use = false;
            }

            previous_mask = Some(event.mask);
        }
    }
}
